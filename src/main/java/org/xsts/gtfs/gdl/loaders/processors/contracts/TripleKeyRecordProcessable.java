package org.xsts.gtfs.gdl.loaders.processors.contracts;

import org.apache.commons.csv.CSVParser;
import org.xsts.core.data.collections.TripleKeyValueList;
/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public interface TripleKeyRecordProcessable {
    void process(CSVParser parser, TripleKeyValueList keyValueList, Object paramOne, Object paramTwo);

}
