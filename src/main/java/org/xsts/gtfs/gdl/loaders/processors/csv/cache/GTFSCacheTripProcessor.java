package org.xsts.gtfs.gdl.loaders.processors.csv.cache;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.gtfs.gdl.loaders.processors.contracts.hidden.DataCacheProcessor;
import org.xsts.gtfs.gds.data.fields.GTFSTripFields;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;
import org.xsts.gtfs.gds.data.types.GTFSTrip;

/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class GTFSCacheTripProcessor extends DataCacheProcessor {
    @Override
    public void process(CSVParser parser, GTFSCache bigCache, Object paramOne, Object paramTwo) {

        for (CSVRecord record : parser) {

            String routeID = record.get(GTFSTripFields.ROUTE);
            if (!bigCache.getLink().containsKey(routeID))
                continue;

            GTFSTrip trip = new GTFSTrip(record, parser);
            bigCache.add(trip.route(), trip.id());
            trip.register();

            trip = null;


        }
    }
}