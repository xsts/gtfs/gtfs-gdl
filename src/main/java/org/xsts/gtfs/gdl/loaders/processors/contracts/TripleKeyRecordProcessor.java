package org.xsts.gtfs.gdl.loaders.processors.contracts;

/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public abstract class  TripleKeyRecordProcessor extends ProgressNotifier implements TripleKeyRecordProcessable{
}
