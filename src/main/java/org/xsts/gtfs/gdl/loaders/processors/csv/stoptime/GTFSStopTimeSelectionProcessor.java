package org.xsts.gtfs.gdl.loaders.processors.csv.stoptime;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.core.data.collections.LinearValueList;
import org.xsts.gtfs.gdl.loaders.processors.contracts.ListRecordProcessor;
import org.xsts.gtfs.gds.data.fields.GTFSStopTimeFields;
import org.xsts.gtfs.gds.data.types.GTFSStopTime;

import java.util.Set;
/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class GTFSStopTimeSelectionProcessor extends ListRecordProcessor {
    @Override
    public void process(CSVParser parser, LinearValueList valueList, Object paramOne, Object paramTwo) {
        Set<String> tripKeys = (Set<String> ) paramOne;

        for (CSVRecord record : parser) {

            String tripID = record.get(GTFSStopTimeFields.TRIP);
            if ( !tripKeys.contains(tripID))
                continue;

            GTFSStopTime stopTime = new GTFSStopTime(record, parser);
            valueList.add(stopTime);
        }
    }
}

