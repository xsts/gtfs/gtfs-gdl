package org.xsts.gtfs.gdl.loaders.processors.contracts;
/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class ProgressNotifier {
    protected int pos = 0;
    protected int percent = 0;
    protected boolean hasProgress = false;

    protected void initProgressNotification(Integer lineCount) {
        if (lineCount != null) {
            percent = lineCount/100;
            if ( percent == 0)
                percent = 1;
            hasProgress = true;
        }
    }

    protected void updateProgress(){
        if (hasProgress) {
            if (pos % percent == 0) {
                int advance = pos / percent;
                notify();
            }
            pos++;
        }
    }

    private void internalUpdate() {

    }
}
