package org.xsts.gtfs.gdl.loaders.processors.csv.stoptime;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.core.data.collections.KeyValueList;
import org.xsts.gtfs.gdl.loaders.processors.contracts.SimpleKeyRecordProcessor;
import org.xsts.gtfs.gds.data.collections.StringToIntegerMap;
import org.xsts.gtfs.gds.data.fields.GTFSStopTimeFields;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;

import java.util.Map;

/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class GTFSStopTimeTripLenProcessor extends SimpleKeyRecordProcessor {
    @Override
    public void process(CSVParser parser, KeyValueList keyValueList, Object paramOne, Object paramTwo) {


        GTFSCache bigCache = GTFSCache.fromGlobal();
        Map<String,String> reverseLink = bigCache.getReverseLink();
        StringToIntegerMap frequencies = (StringToIntegerMap)keyValueList;
        for (CSVRecord record : parser) {

            String tripID = record.get(GTFSStopTimeFields.TRIP);
            String routeID = reverseLink.get(tripID);
            if (routeID == null)
                continue;

            Integer value = frequencies.get(tripID);
            value++;
            frequencies.add(tripID,value);


        }


    }
}
