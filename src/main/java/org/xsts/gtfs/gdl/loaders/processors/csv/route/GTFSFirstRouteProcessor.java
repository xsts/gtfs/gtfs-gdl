package org.xsts.gtfs.gdl.loaders.processors.csv.route;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.core.data.collections.KeyValueList;
import org.xsts.gtfs.gdl.loaders.processors.contracts.SimpleKeyRecordProcessor;
import org.xsts.gtfs.gds.data.types.GTFSRoute;

/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class GTFSFirstRouteProcessor extends SimpleKeyRecordProcessor {
    @Override
    public void process(CSVParser parser, KeyValueList keyValueList, Object paramOne, Object paramTwo) {
        Boolean firstRoute = (Boolean)paramOne;

        for (CSVRecord record : parser) {
            GTFSRoute route = new GTFSRoute(record, parser);
            keyValueList.add(route.id(),route);
            route.register();

            if (firstRoute && !keyValueList.isEmpty())
                break;

        }
    }
}