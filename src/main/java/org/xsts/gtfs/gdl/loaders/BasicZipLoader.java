package org.xsts.gtfs.gdl.loaders;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.io.input.BOMInputStream;
import org.xsts.core.data.collections.KeyValueList;
import org.xsts.gtfs.gdl.loaders.processors.contracts.SimpleKeyRecordProcessor;

import java.io.*;
import java.nio.file.Path;
import java.util.Enumeration;
/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class BasicZipLoader {
    public static KeyValueList load(Path sourcePath,
                                    String fileName,
                                    KeyValueList keyValueList,
                                    SimpleKeyRecordProcessor processor,
                                    Object paramOne,
                                    Object paramTwo){


        //Path thePath = Paths.get(sourcePath.toAbsolutePath().toString(), fileName);
        File realFile = sourcePath.toFile();
        if (realFile.exists()) {

            try (
                    ZipFile archive = new ZipFile(realFile)) {
                for (Enumeration<ZipArchiveEntry> e = archive.getEntries();
                     e.hasMoreElements(); ) {
                    ZipArchiveEntry entry = e.nextElement();
                    if (entry.getName().compareTo(fileName) != 0)
                        continue;

                    InputStream is = archive.getInputStream(entry);
                    // Tricky, I need to read on the fly a chunk of 2048 bytes

                    BufferedInputStream bis = new BufferedInputStream(is);
                    BOMInputStream bomIn = new BOMInputStream(bis);
                    if (bomIn.hasBOM()) {
                        // has a UTF-8 BOM
                    }
                    Reader reader = new InputStreamReader(bomIn);
                    CSVFormat format = CSVFormat.DEFAULT.withHeader().withRecordSeparator("\n");
                    CSVParser parser = new CSVParser(reader, format);

                    processor.process(parser, keyValueList, paramOne, paramTwo);


                    parser.close();
                    reader.close();
                }


            } catch(IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return keyValueList;
    }
}
