package org.xsts.gtfs.gdl.loaders.processors.contracts;

import org.apache.commons.csv.CSVParser;
import org.xsts.core.data.collections.LinearValueList;

/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public interface ListRecordProcessable {
    void process(CSVParser parser, LinearValueList valueList, Object paramOne, Object paramTwo);

}
