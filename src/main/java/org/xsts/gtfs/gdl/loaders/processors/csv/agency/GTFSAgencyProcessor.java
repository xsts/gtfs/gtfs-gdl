package org.xsts.gtfs.gdl.loaders.processors.csv.agency;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.core.data.collections.KeyValueList;
import org.xsts.gtfs.gdl.loaders.processors.contracts.SimpleKeyRecordProcessor;
import org.xsts.gtfs.gds.data.types.GTFSAgency;
/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class GTFSAgencyProcessor  extends SimpleKeyRecordProcessor {
    @Override
    public void process(CSVParser parser, KeyValueList keyValueList, Object paramOne, Object paramTwo) {
        Boolean firstAgency = (Boolean) paramOne;
        for (CSVRecord record : parser) {
            GTFSAgency gtfsAgency = new GTFSAgency(record, parser);
            keyValueList.add(gtfsAgency.id(), gtfsAgency);
            gtfsAgency.register();

        }
    }
}
