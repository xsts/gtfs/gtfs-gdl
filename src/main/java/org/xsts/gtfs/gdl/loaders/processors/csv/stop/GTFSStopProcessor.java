package org.xsts.gtfs.gdl.loaders.processors.csv.stop;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.core.data.collections.KeyValueList;
import org.xsts.gtfs.gdl.loaders.processors.contracts.SimpleKeyRecordProcessor;
import org.xsts.gtfs.gds.data.types.GTFSStop;
/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class GTFSStopProcessor extends SimpleKeyRecordProcessor {
    @Override
    public void process(CSVParser parser, KeyValueList keyValueList, Object paramOne, Object paramTwo) {

        for (CSVRecord record : parser) {

            GTFSStop stop = new GTFSStop(record, parser);
            keyValueList.add(stop.id(),stop);

        }
    }
}
