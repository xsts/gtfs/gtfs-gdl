package org.xsts.gtfs.gdl.loaders.processors.csv.agency;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.core.data.collections.KeyValueList;
import org.xsts.gtfs.gdl.loaders.processors.contracts.SimpleKeyRecordProcessor;
import org.xsts.gtfs.gds.data.types.GTFSAgency;
/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class GTFSAgencyByShortNameProcessor extends SimpleKeyRecordProcessor {
    @Override
    public void process(CSVParser parser, KeyValueList keyValueList, Object paramOne, Object paramTwo) {
        String id = (String)paramOne;
        for (CSVRecord record : parser) {
            GTFSAgency gtfsAgency = new GTFSAgency(record, parser);
            if (id.compareTo(gtfsAgency.id()) != 0)
                continue;
            keyValueList.add(gtfsAgency.id(), gtfsAgency);
            gtfsAgency.register();

            break;
        }
    }
}
