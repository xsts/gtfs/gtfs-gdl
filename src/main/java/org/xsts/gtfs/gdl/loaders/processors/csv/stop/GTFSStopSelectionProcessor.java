package org.xsts.gtfs.gdl.loaders.processors.csv.stop;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.core.data.collections.LinearValueList;
import org.xsts.gtfs.gdl.loaders.processors.contracts.ListRecordProcessor;
import org.xsts.gtfs.gds.data.fields.GTFSStopFields;
import org.xsts.gtfs.gds.data.types.GTFSStop;

import java.util.Set;
/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class GTFSStopSelectionProcessor extends ListRecordProcessor {
    @Override
    public void process(CSVParser parser, LinearValueList valueList, Object paramOne, Object paramTwo) {
        Set<String> stopKeys = (Set<String> ) paramOne;

        for (CSVRecord record : parser) {

            String stopID = record.get(GTFSStopFields.STOP_ID);
            if ( !stopKeys.contains(stopID))
                continue;

            GTFSStop stop = new GTFSStop(record, parser);
            valueList.add(stop);
        }
    }
}
