package org.xsts.gtfs.gdl.loaders.processors.csv.cache;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.gtfs.gdl.loaders.processors.contracts.hidden.DataCacheProcessor;
import org.xsts.gtfs.gds.data.fields.GTFSStopTimeFields;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;
import org.xsts.gtfs.gds.data.types.GTFSStopTime;

import java.util.Map;
import java.util.Set;

public class GTFSCacheStopTimeProcessor extends DataCacheProcessor {
    @Override
    public void process(CSVParser parser, GTFSCache bigCache, Object paramOne, Object paramTwo) {

        Map<String,String> reverseLink = bigCache.getReverseLink();
        Set<String> longestTripSet = (Set<String>)paramOne;
        for (CSVRecord record : parser) {

            String tripID = record.get(GTFSStopTimeFields.TRIP);
            if ( longestTripSet.contains(tripID) == false)
                continue;

            GTFSStopTime stopTime = new GTFSStopTime(record, parser);
            String routeID = reverseLink.get(stopTime.trip());

            bigCache.add(routeID,stopTime.trip(),stopTime);

        }
    }
}
