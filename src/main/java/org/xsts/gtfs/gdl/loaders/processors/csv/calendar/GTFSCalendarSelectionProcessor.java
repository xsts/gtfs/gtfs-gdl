package org.xsts.gtfs.gdl.loaders.processors.csv.calendar;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.core.data.collections.LinearValueList;
import org.xsts.gtfs.gdl.loaders.processors.contracts.ListRecordProcessor;
import org.xsts.gtfs.gds.data.fields.GTFSCalendarFields;
import org.xsts.gtfs.gds.data.types.GTFSCalendar;

import java.util.Set;
/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class GTFSCalendarSelectionProcessor extends ListRecordProcessor {
    @Override
    public void process(CSVParser parser, LinearValueList valueList, Object paramOne, Object paramTwo) {
        Set<String> serviceKeys = (Set<String>) paramOne;

        for (CSVRecord record : parser) {

            String serviceID = record.get(GTFSCalendarFields.SERVICE);
            if (!serviceKeys.contains(serviceID))
                continue;

            GTFSCalendar calendar = new GTFSCalendar(record, parser);
            valueList.add(calendar);
        }
    }
}
