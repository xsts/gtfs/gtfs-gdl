package org.xsts.gtfs.gdl.loaders.processors.contracts;

import org.apache.commons.csv.CSVParser;
import org.xsts.core.data.collections.KeyValueList;
/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public interface SimpleKeyRecordProcessable {
    void process(CSVParser parser, KeyValueList keyValueList, Object paramOne, Object paramTwo);
}
