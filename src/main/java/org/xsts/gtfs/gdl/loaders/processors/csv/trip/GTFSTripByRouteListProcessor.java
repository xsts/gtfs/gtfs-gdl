package org.xsts.gtfs.gdl.loaders.processors.csv.trip;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.core.data.collections.TripleKeyValueList;
import org.xsts.gtfs.gdl.loaders.processors.contracts.TripleKeyRecordProcessor;
import org.xsts.gtfs.gds.data.collections.GTFSRouteList;
import org.xsts.gtfs.gds.data.types.GTFSTrip;

public class GTFSTripByRouteListProcessor extends TripleKeyRecordProcessor {
    @Override
    public void process(CSVParser parser, TripleKeyValueList tripleKeyValueList, Object paramOne, Object paramTwo) {
        GTFSRouteList routelist = (GTFSRouteList) paramOne;
        for (CSVRecord record : parser) {

            GTFSTrip trip = new GTFSTrip(record, parser);
            String routeID = trip.route();
            if ( routelist.get(routeID) == null)
                continue;
            tripleKeyValueList.add(trip.route(), trip.service(),trip.id(), trip);
        }
    }
}
