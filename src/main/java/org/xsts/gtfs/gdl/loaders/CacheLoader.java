package org.xsts.gtfs.gdl.loaders;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.io.input.BOMInputStream;
import org.xsts.gtfs.gdl.loaders.processors.contracts.hidden.DataCacheProcessor;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Path;
import java.nio.file.Paths;
/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class CacheLoader {
    public static void load(Path sourcePath,
                            String fileName,
                            GTFSCache bigCache,
                            DataCacheProcessor processor,
                            Object paramOne,
                            Object paramTwo){
        Path thePath = Paths.get(sourcePath.toAbsolutePath().toString(), fileName);
        File realFile = thePath.toFile();
        if (realFile.exists()) {
            try {
                BOMInputStream bomIn = new BOMInputStream(new FileInputStream(thePath.toString()));
                if (bomIn.hasBOM()) {
                    // has a UTF-8 BOM
                }
                Reader reader = new InputStreamReader(bomIn);
                CSVFormat format = CSVFormat.DEFAULT.withHeader().withRecordSeparator("\n");
                CSVParser parser = new CSVParser(reader, format);

                processor.process(parser, bigCache, paramOne, paramTwo);

                parser.close();
                reader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
