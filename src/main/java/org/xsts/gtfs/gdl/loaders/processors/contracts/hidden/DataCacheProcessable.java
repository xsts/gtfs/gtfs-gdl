package org.xsts.gtfs.gdl.loaders.processors.contracts.hidden;
/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.apache.commons.csv.CSVParser;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;

public interface DataCacheProcessable {
    void process(CSVParser parser, GTFSCache bigCache, Object paramOne, Object paramTwo);
}
