package org.xsts.gtfs.gdl.loaders;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.io.input.BOMInputStream;
import org.xsts.core.data.collections.KeyValueList;
import org.xsts.core.data.collections.LinearValueList;
import org.xsts.core.data.collections.TripleKeyValueList;
import org.xsts.gtfs.gdl.loaders.processors.contracts.ListRecordProcessor;
import org.xsts.gtfs.gdl.loaders.processors.contracts.SimpleKeyRecordProcessor;
import org.xsts.gtfs.gdl.loaders.processors.contracts.TripleKeyRecordProcessor;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Path;
import java.nio.file.Paths;
/*
 * Group : XSTS/GTFS
 * Project : GTFS Data Loaders
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class BasicDirLoader {
    public static KeyValueList load(Path sourcePath,
                                    String fileName,
                                    KeyValueList keyValueList,
                                    SimpleKeyRecordProcessor processor,
                                    Object paramOne,
                                    Object paramTwo){
        Path thePath = Paths.get(sourcePath.toAbsolutePath().toString(), fileName);
        File realFile = thePath.toFile();
        if (realFile.exists()) {
            try {
                BOMInputStream bomIn = new BOMInputStream(new FileInputStream(thePath.toString()));
                if (bomIn.hasBOM()) {
                    // has a UTF-8 BOM
                }
                Reader reader = new InputStreamReader(bomIn);
                CSVFormat format = CSVFormat.DEFAULT.withHeader().withRecordSeparator("\n").withIgnoreSurroundingSpaces(true);
                CSVParser parser = new CSVParser(reader, format);

                processor.process(parser, keyValueList, paramOne, paramTwo);

                parser.close();
                reader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return keyValueList;
    }


    public static TripleKeyValueList load(Path sourcePath,
                                          String fileName,
                                          TripleKeyValueList tripleKeyValueList,
                                          TripleKeyRecordProcessor processor,
                                          Object paramOne,
                                          Object paramTwo){
        Path thePath = Paths.get(sourcePath.toAbsolutePath().toString(), fileName);
        File realFile = thePath.toFile();
        if (realFile.exists()) {
            try {
                BOMInputStream bomIn = new BOMInputStream(new FileInputStream(thePath.toString()));
                if (bomIn.hasBOM()) {
                    // has a UTF-8 BOM
                }
                Reader reader = new InputStreamReader(bomIn);
                CSVFormat format = CSVFormat.DEFAULT.withHeader().withRecordSeparator("\n");
                CSVParser parser = new CSVParser(reader, format);

                processor.process(parser, tripleKeyValueList, paramOne, paramTwo);

                parser.close();
                reader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return tripleKeyValueList;
    }

    public static LinearValueList load(Path sourcePath,
                                       String fileName,
                                       LinearValueList linearValueList,
                                       ListRecordProcessor processor,
                                       Object paramOne,
                                       Object paramTwo){
        Path thePath = Paths.get(sourcePath.toAbsolutePath().toString(), fileName);
        File realFile = thePath.toFile();
        if (realFile.exists()) {
            try {
                BOMInputStream bomIn = new BOMInputStream(new FileInputStream(thePath.toString()));
                if (bomIn.hasBOM()) {
                    // has a UTF-8 BOM
                }
                Reader reader = new InputStreamReader(bomIn);
                CSVFormat format = CSVFormat.DEFAULT.withHeader().withRecordSeparator("\n");
                CSVParser parser = new CSVParser(reader, format);

                processor.process(parser, linearValueList, paramOne, paramTwo);

                parser.close();
                reader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return linearValueList;
    }
}
