package org.xsts.gtfs.gdl.loaders.processors.csv.cache;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.gtfs.gdl.loaders.processors.contracts.hidden.DataCacheProcessor;
import org.xsts.gtfs.gds.data.collections.GTFSStopList;
import org.xsts.gtfs.gds.data.fields.GTFSStopTimeFields;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;
import org.xsts.gtfs.gds.data.types.GTFSStop;

public class GTFSCacheTripSignatureProcessor extends DataCacheProcessor {

    @Override
    public void process(CSVParser parser, GTFSCache bigCache, Object paramOne, Object paramTwo) {
        Integer lineCount = (Integer) paramTwo;
        int percent = lineCount / 100;
        if (percent == 0)
            percent = 1;
        int pos = 0;
        GTFSStopList stops = (GTFSStopList) paramOne;
        for (CSVRecord record : parser) {

            String tripID = record.get(GTFSStopTimeFields.TRIP);
            if (!bigCache.getReverseLink().containsKey(tripID))
                continue;

            String stopID = record.get(GTFSStopTimeFields.STOP);
            GTFSStop stop = stops.get(stopID);
            String sequence = record.get(GTFSStopTimeFields.SEQUENCE);

            // Need to convert 1, 01, 001, 0001 into 1
            Integer tempValue = Integer.parseInt(sequence);
            sequence = tempValue.toString();
            bigCache.registerTripStopSequence(tripID, stop.uid(), sequence);

        }
    }
}